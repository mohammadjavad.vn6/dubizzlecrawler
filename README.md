# [Dubbizzle Crawler](https://bit.ly/37Nkqny):
This project has few concepts, gather some information from **Dubizzle** website and store them in **MySQL** Database.
In this project **PEEWEE** is used as ORM.
Gathered Information is limited to **Job** category in **Dubai**.

*Doc* : https://bit.ly/37Nkqny

### This project is supposed to gather below information:

*	date
*	company name
*	employment type
*	monthly salary
*	job role
*   benefits
*	minimum work experience
*	minimum education level
*	company size
*	career level
*	description
*	links
*	title

## Installing and Getting Started:

1. Create a database in MySQL as "Dubizzle"

```shell script
create database Dubizzle;
```
2. Choose **"utf8 - default collation"** as default collation of your DataBase 

```shell script
ALTER SCHEMA `Dubizzle`  DEFAULT CHARACTER SET utf8 ;
```
3. Create a dictionary in local_config file like:

```shell script
DB_CONFIG = {"user" : "Your username",
            "password" : "Your password",
            "host" : "Your host",
            "port" : "Your port as a integer like 3306"
}
```
4. Install requirements from requirements.txt

```shell script
pip install -r requirements.txt
```
5. Run main.py and let`s crawl!

```shell script
python main.py
```
## Built With:
* [PEEWEE](http://docs.peewee-orm.com/en/latest/) - ORM USED
* [MySQL](https://www.mysql.com/) - Used MySQL database to store data
* [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) - Used to parse HTMLs
* [Requests](https://requests.readthedocs.io/en/master/) - Used for make HTTP requests to get data from web site

