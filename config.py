from peewee import MySQLDatabase
from local_config import DB_CONFIG

db = MySQLDatabase(
    "Dubizzle", **DB_CONFIG)


url = "https://dubai.dubizzle.com" \
      "/jobs/search/?page={}&keywords=&is_search=1&is_basic_search_widget=1"

headers = {
    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0)"
    "Gecko/20100101 Firefox/72.0"
}
