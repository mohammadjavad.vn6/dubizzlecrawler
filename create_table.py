from logger import file_logger
from models.base import BaseModel
from config import db


def create_tables():
    models = BaseModel.__subclasses__()
    db.create_tables(models)
    file_logger.info("Tables created")


create_tables()
