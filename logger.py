import logging


file_logger = logging.getLogger(__name__)
file_logger.setLevel(logging.DEBUG)

f_handler = logging.FileHandler(filename="DubiizleCrawler.log")
f_handler.setLevel(logging.DEBUG)

f_formatter = logging.Formatter(
    "[%(threadName)s][%(asctime)s][%(levelname)s][%(name)s][%(funcName)s]: [%(message)s]"
)
f_handler.formatter = f_formatter

file_logger.addHandler(f_handler)
