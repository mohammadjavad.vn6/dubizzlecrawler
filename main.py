from create_table import create_tables
from logger import file_logger
from thread import main_request, main2, put_in_queue

create_key = input("create tables?")
if __name__ == "__main__":
    if create_key.lower() == "Y".lower():
        create_tables()
    main_request()
    file_logger.info("crawl started")
    put_in_queue()
    main2()
