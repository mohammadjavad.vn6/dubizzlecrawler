from peewee import CharField
from models.base import BaseModel


class Link(BaseModel):
    link = CharField(unique=True)
    title = CharField()
