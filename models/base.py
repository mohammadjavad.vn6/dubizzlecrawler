from datetime import datetime
from config import db
from peewee import Model, DateTimeField


class BaseModel(Model):
    created_time = DateTimeField(default=datetime.now())

    class Meta:
        database = db
