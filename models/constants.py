EMPLOYMENT_TYPE = (
    ("f", "full time"),
    ("p", "part time"),
    ("c", "contract"),
    ("t", "temporary"),
    ("o", "other")
)

CAREER_LEVEL = (
    ("e", "executive/director"),
    ("ml", "manager"),
    ("s", "senior"),
    ("j", "junior"),
    ("m", "mid-level"),

)

MINIMUM_EDUCATION_LEVEL = (
    ("p", "phd degree"),
    ("m", "master degree"),
    ("b", "bachelor degree"),
    ("h", "high-school/secondary"),
    ("n", "N/A")
)


MINIMUM_WORK_EXPERIENCE = (
    (0, "0-1 years"),
    (1, "1-2 years"),
    (2, "2-5 years"),
    (5, "5-10 years"),
    (10, "10-15 years"),
    (15, "15+ years")
)

MONTHLY_SALARY = (
    (0, "unspecified"),
    (1, "0-1,999 AED"),
    (2, "2,000-3,999 AED"),
    (4, "4,000-5,999 AED"),
    (6, "6,000-7,999 AED"),
    (8, "8,000-11,999"),
    (12, "12,000-19,999 AED"),
    (20, "20,000-29,999 AED"),
    (30, "30,000-49,999 AED"),
    (50, "50,000-99,999 AED"),
    (100, "100,000+ AED")
)

COMPANY_SIZE = (
    (1, "1-10"),
    (2, "11-50"),
    (3, "51-200"),
    (4, "201-500"),
    (5, "501-1000"),
    (6, "1001-5000"),
    (7, "5001-10000"),
    (8, "10000+")
)
