from peewee import DateField, CharField, IntegerField, TextField, \
    ForeignKeyField
from models.Links import Link
from models.base import BaseModel
from models.constants import EMPLOYMENT_TYPE, MONTHLY_SALARY, \
    MINIMUM_WORK_EXPERIENCE, MINIMUM_EDUCATION_LEVEL, COMPANY_SIZE, \
    CAREER_LEVEL


class Detail(BaseModel):
    date = DateField(formats=["%Y-%m-%d"])
    company_name = CharField(null=True)
    employment_type = CharField(max_length=1, choices=EMPLOYMENT_TYPE)
    monthly_salary = IntegerField(choices=MONTHLY_SALARY)
    job_role = CharField(null=True)
    benefits = CharField(null=True)
    minimum_work_experience = IntegerField(
        choices=MINIMUM_WORK_EXPERIENCE, null=True
    )
    minimum_education_level = CharField(
        max_length=1, choices=MINIMUM_EDUCATION_LEVEL, null=True
    )
    listed_by = CharField(null=True)
    company_size = IntegerField(choices=COMPANY_SIZE, null=True)
    career_level = CharField(max_length=1, choices=CAREER_LEVEL)
    description = TextField()
    short_link = CharField()
    link_id = ForeignKeyField(Link, unique=True)
