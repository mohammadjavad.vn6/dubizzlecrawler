import re
from bs4 import BeautifulSoup

from logger import file_logger
from models.Links import Link
from store import save_link_to_sql, save_adv


def parser(response):
    content = BeautifulSoup(response)
    find_link_title(content)


def find_link_title(content):
    for url in content.find_all('span', class_="title"):
        link = url.find("a")['href']
        title = url.text
        file_logger.info("{} parsed and saved in DB".format(link))
        save_link_to_sql(link, title)


def adv_links_parser(resp):
    response = BeautifulSoup(resp.content)
    date = response.find("span", itemprop="datePosted")["content"]
    print(date)
    company_name = response.find("strong", itemprop="hiringOrganization")
    if company_name is not None:
        company_name = company_name.get_text(strip="n")
    else:
        company_name = None
    employment_type = response.find(
        "strong", itemprop="employmentType").get_text(strip="n")
    monthly_salary = response.find(
        "span", string=re.compile("Monthly Salary:")
    )
    if monthly_salary is not None:
        monthly_salary = monthly_salary.find_next_sibling(
            "strong").get_text(strip=" ").split("\n")[0]
    job_role = response.find("span", string=re.compile("Job Role"))
    if job_role is not None:
        job_role = job_role.find_next_sibling("strong").get_text(strip=" ")
    else:
        job_role = None
    benefits = response.find("strong", itemprop="benefits")
    if benefits is not None:
        benefits = benefits.get_text(strip="n")
    else:
        benefits = None
    minimum_work_experience = response.find(
        "strong", itemprop="experienceRequirements").get_text(strip="n")
    minimum_education_level = response.find(
        "strong", itemprop="educationRequirements").get_text(strip="n")
    listed_by = response.find("span", string=re.compile("Listed By:"))
    if listed_by is not None:
        listed_by = listed_by.find_next_sibling("strong").get_text(strip=" ")
    else:
        listed_by = None
    company_size = response.find("span", string=re.compile("Company Size:"))
    if company_size is not None:
        company_size = company_size.find_next_sibling(
            "strong").get_text(strip=" ").split("\n")[0]
    else:
        company_size = None
    career_level = response.find("span", string=re.compile("Career Level:"))
    if career_level is not None:
        career_level = career_level.find_next_sibling(
            "strong").get_text(strip=" ")
    description = response.find("span", itemprop="description").get_text()
    short_link = response.find("input", id="short-link-input")["value"]

    file_logger.error(
        "There is something wrong with parsing {}".format(resp.url)
    )

    adv_fields = {
        "date": date, "company_name": company_name,
        "employment_type": employment_type,
        "monthly_salary": monthly_salary, "job_role": job_role,
        "benefits": benefits,
        "minimum_work_experience": minimum_work_experience,
        "minimum_education_level": minimum_education_level,
        "listed_by": listed_by, "company_size": company_size,
        "career_level": career_level, "description": description,
        "short_link": short_link,
        "link_id": Link.select().where(Link.link == resp.url).get()
    }

    file_logger.info("{} crawled successfully".format(resp.url))

    save_adv(adv_fields)
