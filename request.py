from queue import Queue
from time import sleep
import requests
from config import url, headers
from parser import parser, adv_links_parser
from random import choice
from logger import file_logger
import threading


class ReqLink(threading.Thread):

    page = 1
    queue = Queue()
    lock = threading.RLock()

    def __init__(self):
        threading.Thread.__init__(self)
        self.link = url

    def run(self):
        crawl = True
        while crawl:

            self.lock.acquire()
            link = self.link.format(self.page)
            ReqLink.page += 1
            self.lock.release()

            if not link in self.queue.queue:
                response = requests.get(link, headers=headers)
                self.queue.put(link)

            crawl = bool(response.status_code == 200)
            if crawl:
                file_logger.debug("Page {} crawled".format(self.page))
                parser(response.content)
            else:
                file_logger.info("Crawl finished")
                file_logger.debug("status code:".format(response.status_code))
                print("finish :\nstatus_code: ", response.status_code)


def request_to_each_link(queue):
    while bool(queue.qsize()):
        link = queue.get()
        request_to_each_single_link(link)
        queue.task_done()


def request_to_each_single_link(link):
    delay_list = [3, 4, 5]
    sleep(choice(delay_list))
    response = requests.post(link, verify=False, headers=headers)
    file_logger.info("{} ready to crawl".format(response.url))
    file_logger.error(
        "{} gave {} status code".format(response.url, response.status_code)
    )
    adv_links_parser(response)
