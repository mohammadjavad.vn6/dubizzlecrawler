from peewee import IntegrityError, InternalError
from logger import file_logger
from models.Links import Link
from models.details import Detail


def save_link_to_sql(url, title):
    try:
        Link.get_or_create(link=url, title=title)
    except IntegrityError:
        print("it exists")
        pass
    except InternalError:
        pass


def get_link_from_database():
    urls_list = list()
    for url in Link.select(Link.link):
        urls_list.append(url.link)
    return urls_list


def save_adv(adv_fields):
    try:
        Detail.get_or_create(**adv_fields)
        file_logger.info("Data saved in DB successfully")
        file_logger.error("There is something wrong with saving data")
    except IntegrityError:
        print("it is in there")
        pass
    except InternalError:
        pass
