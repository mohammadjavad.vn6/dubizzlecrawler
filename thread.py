import threading
from queue import Queue

from logger import file_logger
from request import request_to_each_link, ReqLink
from store import get_link_from_database


def main_request():
    threads_list = list()
    for adv_urls in range(10):
        x = ReqLink()
        threads_list.append(x)
        x.start()
    for j in threads_list:
        j.join()


queue = Queue()


def put_in_queue():
    for url in get_link_from_database():
        queue.put(url)


def main2():
    threads_list2 = list()
    for url in range(10):
        y = threading.Thread(target=request_to_each_link, args=(queue,))
        threads_list2.append(y)
        y.start()
    for s in threads_list2:
        s.join()
    file_logger.debug("finished")
